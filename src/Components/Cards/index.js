import React, { useState } from "react";
import heart from "../../Assets/Images/heart.png";
import comment from "../../Assets/Images/comments.png";
import share from "../../Assets/Images/share.png";
import heartfilled from "../../Assets/Images/heartfilled.png";

function Cards({ posts,socket,user }) {
  const [clicked, isClicked] = useState(false);
 
  const addnotification = (type) => {
    
     isClicked(true);
    console.log(posts.username,"======user=======")
    socket.emit("sendNotification",{
      senderName:user,
      receiverName:posts.username,
      type
    })
  };

  return (
    <div>
     
          <div  className="card">
            <div className="info">
              <img src={posts.userImg} alt="" className="userImg" />
              <span className="fullname">{posts.fullname}</span>
            </div>
            <img src={posts.postImg} alt="" className="postImg" />
            <div className="interactions">
              {clicked ? (
                <img
                  src={heartfilled}
                  alt=""
                  className="interaction-icon"
                  onClick={() => isClicked(false)}
                />
              ) : (
                <img
                  src={heart}
                  alt=""
                  className="interaction-icon"
                  onClick={() => addnotification(1)}
                />
              )}

              <img src={comment} alt="" className="interaction-icon"  onClick={() => addnotification(2)}/>
              <img src={share} alt="" className="interaction-icon"  onClick={() => addnotification(3)}/>
            </div>
          
          </div>
      
    </div>
  );
}

export default Cards;
