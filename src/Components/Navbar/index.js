import React, { useEffect, useState } from "react";
import "../../css/style.css";
import notification from "../../Assets/Images/notification.png";
import mail from "../../Assets/Images/mail.png";
import settings from "../../Assets/Images/settings.png";

function Navbar({ socket }) {
  const [notifications, setNotifications] = useState([]);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    socket.on("getNotification", (data) => {
      setNotifications((prev) => [...prev, data]);
    });
  }, [socket]);

  const displayNotification = ({ senderName, type }) => {
    let action;

    if (type === 1) {
      action = "liked";
    } else if (type === 2) {
      action = "commented";
    } else {
      action = "shared";
    }
    return (
      notifications?
      
        
      <span className="notification">{`${senderName} ${action} your post.`}</span>:
      <span className="notification">No notifications for you</span>
    
    );
  };

  const handleRead = () => {
    setNotifications([]);
    setOpen(false);
  };

  return (
    <div className="navbar">
      <span className="logo">Practice</span>
      <div className="icons">
        <div className="icon" onClick={() => setOpen(!open)}>
          <img className="IconImg" src={notification} alt="" />
          {notifications.length > 0 && (
            <div className="counter">{notifications.length}</div>
          )}
        </div>
        <div className="icon" onClick={() => setOpen(!open)}>
          <img className="IconImg" src={mail} alt="" />
          <span className="count">2</span>
        </div>
        <div className="icon" onClick={() => setOpen(!open)}>
          <img className="IconImg" src={settings} alt="" />
          <span className="count">2</span>
        </div>
        {open && (
        <div className="notifications">
          {notifications.map((n) => displayNotification(n))}
          {notifications ?
          <button className="nButton" onClick={handleRead}>
            
            Mark as read
          </button>:
          <button disabled className="nButton">
            
          Mark as read
        </button>}
        </div>
      )}
      </div>
    </div>
  );
}

export default Navbar;
