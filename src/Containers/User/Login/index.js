import React, { useEffect, useState } from "react";
import "../../../css/style.css";
import Navbar from "../../../Components/Navbar";
import Cards from "../../../Components/Cards";
import {posts} from '../../../Assets/data'
import {io} from 'socket.io-client'
function Login() {
  const [userName, setUserName] = useState("");
  const [user, setUser] = useState("");
  const [socket,setSocket]= useState(null)
useEffect(()=>{
 
  setSocket(io(process.env.REACT_APP_SOCKET_URL))
  
},[])

useEffect(()=>{
  socket?.emit("newUser",user)
},[socket,user])

  const userSubmit = (data) => {
    setUser(data);
    console.log(data, "username");
  };

  return (
    <div className="container">
      {!user ? (
        <div className="login">
          <input
            className="username"
            placeholder="Enter username"
            onChange={(e) => setUserName(e.target.value)}
          />
          <button onClick={() => userSubmit(userName)} className="login-button">
            Submit
          </button>
        </div>
      ) : (
        <div >
          <Navbar socket={socket}/>
          {posts?.map((posts) => {
        return (
          <Cards posts={posts} key={posts.id} socket={socket} user={user}/>
          );
        })}
          <span className="user-info">{user}</span>
        </div>
      )}
    </div>
  );
}

export default Login;
